var NS="http://www.w3.org/2000/svg";
var draw;
var background_img;
var marker;
var imgPath = "images/DashBoard/";

var pieChart1;
var pieChart2;
var borderColor = "";
var shopId = 1;
var totalOperationRatio = 0;

$(function() {
	
	draw = SVG("svg");
		
	var timeStamp = new Date().getMilliseconds();
	
	getMachineInfo();
});

function printMachineName(arrayIdx, x, y, w, h, name, color, fontSize, dvcId, notUse){
	
/* 원래 코드 */	
	machineName[arrayIdx] = draw.text(nl2br(name)).fill(color);
	machineName[arrayIdx].font({
		  family:   'Helvetica'
		, size:     fontSize
		, anchor:   'middle'
		, leading:  '0em'
		, weight: 'bold'
			,fill: color
	});
	
	machineName[arrayIdx].dblclick(function(){
		if(dvcId!=0){
			window.sessionStorage.setItem("dvcId", dvcId);
			window.sessionStorage.setItem("name", name);
			window.sessionStorage.removeItem("date");
			
			window.localStorage.setItem("dvcId",dvcId);
			
			location.href="/Single_Chart_Status/index.do?lang=" + window.localStorage.getItem("lang");
		};
	})
	
	var wMargin = 0;
	var hMargin = 0;
	
	// 장비 명 출력 위치 조정 
	if(arrayIdx == 0  || arrayIdx == 1  || arrayIdx == 2  || arrayIdx == 6  || arrayIdx == 7  || arrayIdx == 8) { // dvcId : 1,2,3,7,8,9
		machineName[arrayIdx].x(x + (w/2) - wMargin);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10) + 20);
	
	} else if (arrayIdx == 3 || arrayIdx == 4 || arrayIdx == 5  || arrayIdx == 9  || arrayIdx == 10  || arrayIdx == 11) { // dvcId : 4,5,6,10,11,12
		machineName[arrayIdx].x(x + (w/2) - wMargin);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10) - 15);
	} else if (arrayIdx == 12 || arrayIdx == 14){	// dvcId : 13,15
		machineName[arrayIdx].x(x + (w/2) - wMargin);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10) + 20);
	} else if (arrayIdx == 13 || arrayIdx == 15){	// dvcId : 14,16
		machineName[arrayIdx].x(x + (w/2) - wMargin);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10) - 5);
	} else {
		machineName[arrayIdx].x(x + (w/2) - wMargin);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10));
	} 
	
	machineName[arrayIdx].leading(1);
};


function nl2br(value) { 
	  return value.replace(/<br>/g, "\n");
};

var machineProp = new Array();
var machineList = new Array();
var machineName = new Array();
var machineStatus = new Array();
var newMachineStatus = new Array();
var machineArray2 = new Array();
var first = true;


function getMachineInfo(){
	var url = ctxPath + "/svg/getMachineInfo.do";
	var param = "shopId=" + shopId;
	
	//console.log(param)
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			//console.log(data)
			$(".name").remove();
			
			var json = data.machineList;
			newMachineStatus = new Array();
			machineProp = [];
			noconn = [];
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;

			operationTime = 0;
			
			$(".wifi").remove()
			
			
			
			
			$(json).each(function(key, data){
				var array = new Array();
				var machineColor = new Array();
				
//				console.log("data 확인 : " + data)
//				console.log("좌표 확인 ie_x : " + data.ie_x);
//				console.log("좌표 확인 ie_y : " + data.ie_y);
//				console.log("크기 확인 w : " + data.w);
//				console.log("크기 확인 h : " + data.h);
//				console.log("pic 확인 pic : " + data.pic);
//				
//				console.log("이름 확인 : " + data.name);
//				console.log("길 확인 : " + data.notUse);
				
				
				array.push(data.id);
				
				
				// jane test
				var data_nm = data.name
				
				if(data_nm.includes("%23")) {
					data_nm = data_nm.split("%23")[0] + "<br>" + "#" + data_nm.split("%23")[1]
				}
				//
				
				array.push(data_nm);
				
				//console.log("이름 확인 : " + data_nm);				
				//console.log("lastChartStatus : " + data.lastChartStatus);
				
				array.push(getElSize(data.x));
				array.push(getElSize(data.y));
				array.push(getElSize(data.w));
				array.push(getElSize(data.h));
				array.push(data.pic);
				array.push(data.lastChartStatus);
				array.push(data.dvcId);
				array.push(getElSize(data.fontSize));
				
				
				if(data.notUse==1){
					array.push("NOTUSE");
					 powerOffMachine--;
				}else{
					array.push(data.lastChartStatus);
				}
				array.push(data.notUse);
				
				
				operationTime += Number(data.operationTime);
				
				if(data.lastChartStatus=="IN-CYCLE" || data.lastChartStatus=="IN-CYCLE-BLINK"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT" || data.lastChartStatus=="WAIT-NOBLINK"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM" || data.lastChartStatus=="ALARM-NOBLINK" ){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
					noconn.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"))
				};
				
				
				var text_color;
				if(data.lastChartStatus=="WAIT" || data.lastChartStatus=="NO-CONNECTION"){
					text_color = "#000000";
				}else{
					text_color = "#ffffff";
				};
				
				
				//idx, x, y, w, h, name
				
			//	printMachineName(null, getElSize(data.x), getElSize(data.y), getElSize(data.w), getElSize(data.h), replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"), text_color, getElSize(data.fontSize), data.dvcId, replaceAll(replaceAll(decodeURI(data.cnt), "%23","#"),"%2F","/"));
				
				
				var img_width = getElSize(30);
				if(String(data.type).indexOf("IO") == -1 && data.type != null){
					var wifi = document.createElement("img");
					wifi.setAttribute("src", ctxPath + "/images/wifi.png");
					wifi.setAttribute("class", "wifi");
					
					wifi.style.cssText = "position : absolute" + 
										"; width : " + img_width + "px" +
										"; height : " + img_width + "px" +
										"; border-radius :50%" + 
										"; top: " + ($("#n" + data.dvcId).offset().top - img_width) + "px" + 
										"; z-index : " + 9 +
										"; left : " + $("#n" + data.dvcId).offset().left + "px"; 
					
					$("body").append(wifi)
					
					
					if(data.isChg){
						if(data.chgTy=="O"){
							var offset_chg = document.createElement("img");
							offset_chg.setAttribute("src", ctxPath + "/images/offset_chg.png");
							offset_chg.setAttribute("class", "wifi");
							
							offset_chg.style.cssText = "position : absolute" + 
												"; width : " + img_width + "px" +
												"; height : " + img_width + "px" +
												"; border-radius :50%" + 
												"; z-index : " + 9 +
												"; top: " + ($("#n" + data.dvcId).offset().top - img_width) + "px" +
												"; left : " + ($("#n" + data.dvcId).offset().left + $("#n" + data.dvcId).width() - img_width) + "px";
							
							$("body").append(offset_chg)
							
						}else{
							var condition_chg = document.createElement("img");
							condition_chg.setAttribute("src", ctxPath + "/images/condition_chg.png");
							condition_chg.setAttribute("class", "wifi");
							
							condition_chg.style.cssText = "position : absolute" + 
												"; width : " + img_width + "px" +
												"; height : " + img_width + "px" +
												"; border-radius :50%" + 
												"; z-index : " + 9 +
												"; top: " + ($("#n" + data.dvcId).offset().top - img_width) + "px" +
												"; left : " + ($("#n" + data.dvcId).offset().left + $("#n" + data.dvcId).width() - img_width) + "px";
							
							$("body").append(condition_chg)
						}
					}
				} 
				
				
				
				machineProp.push(array);
				
				machineColor.push(data.id);
				machineColor.push(data.lastChartStatus);
				
				newMachineStatus.push(machineColor);
			});
			
			if(!compare(machineStatus,newMachineStatus)){
				reDrawMachine();
			};
			
			if(first){
				for(var i = 0; i < machineProp.length; i++){
					drawMachine(machineProp[i][0],
							machineProp[i][1],
							machineProp[i][2],
							machineProp[i][3],
							machineProp[i][4],
							machineProp[i][5],
							machineProp[i][6],
							machineProp[i][7],
							i,
							machineProp[i][8],
							machineProp[i][9],
							machineProp[i][10]);
				};
				first = false;
			};
			
			totalOperationRatio = 0;
			for(var i = 0; i < machineArray2.length; i++){
				totalOperationRatio += machineArray2[i][20];
			};
		}, erorr : (e1,e2,e3)=>{
			console.log(e1)
			console.log(e2)
		}
	});
	
	
	// 주기 3초(3000) 마다 페이지 갱신 : 장비 상태 갱신
	setTimeout(getMachineInfo, 3000);
};

function replaceAll(str, src, target){
	return str.split(src).join(target)
};

function reDrawMachine(){
	for(var i = 0; i < machineStatus.length; i++){
		if(machineStatus[i][1]!=newMachineStatus[i][1]){
			machineList[i].remove();
			
			//array = id, name, x, y, w, h, pic, status
			
			drawMachine(machineProp[i][0],
					machineProp[i][1],
					machineProp[i][2],
					machineProp[i][3],
					machineProp[i][4],
					machineProp[i][5],
					machineProp[i][6],
					newMachineStatus[i][1],
					i,
					machineProp[i][8],
					machineProp[i][9],
					machineProp[i][10]);
		}
	};
	
	machineStatus = newMachineStatus;
};

function compare ( a, b ){
	var type = typeof a, i, j;
	 
	if( type == "object" ){
		if( a === null ){
			return a === b;
		}else if( Array.isArray(a) ){ //배열인 경우
			//기본필터
	      if( !Array.isArray(b) || a.length != b.length ) return false;
	 
	      //요소를 순회하면서 재귀적으로 검증한다.
	      for( i = 0, j = a.length ; i < j ; i++ ){
	        if(!compare(a[i], b[i]))return false;
	      	};
	      return true;
	    };
	  };
	 
	  return a === b;
};

function getMarker(){
	var url = ctxPath + "/svg/getMarker.do";
	
	$.ajax({
		url : url,
		type: "post",
		dataType : "json",
		success : function(data){
			marker = draw.image(imgPath + data.pic + ".png").size(data.w, data.h);
			marker.x(data.x);
			marker.y(data.y);
			
			
			//marker.draggable();
			marker.dragmove  = function(delta, evt){
				var id = data.id;
				var x = marker.x();
				var y = marker.y();
				
				//DB Access
				setMachinePos(id, x, y);
			};
		}
	});
};



var table_8 = true


function drawMachine(id, name, x, y, w, h, pic, status, i, dvcId, fontSize, notUse){
	//alert(dvcId + ", " + name + ", " + notUse);
	var svgFile;
	var timeStamp = new Date().getMilliseconds();
	if(status==null){
		svgFile = ".svg";
	}else{
		//if(status=="ALARM" || name.indexOf("HFMS")!=-1) status = "IN-CYCLE";
		svgFile = "-" + status + ".svg";
	};
	
	//console.log(machineList);
	
	//wilson
	machineList[i] = draw.image(imgPath + pic + svgFile + "?dummy=" + timeStamp).size(w, h);
	machineList[i].x(x);
	machineList[i].y(y);
	
	
	//console.log(machineList);
	//console.log("x, y : " + machineList[i].x(x) + ", " + x +  machineList[i].y(y) + ", " + y);
	
	var text_color;
	
	// textColor
	if(status=="IN-CYCLE-BLICK" || status=="IN-CYCLE"){
		text_color = "#ffffff";
	}else{
		text_color = "#000000";
	};
	
	fontSize = getElSize(35);
	
	printMachineName(i, x, y, w, h, name, text_color, fontSize, dvcId, notUse);
};

function setDraggable(arrayIdx, id, w, h){
	machineList[arrayIdx].draggable();
	
	machineList[arrayIdx].dragmove  = function(delta, evt){
		var x = machineList[arrayIdx].x();
		var y = machineList[arrayIdx].y();
		
		//DB Access
		setMachinePos(id, setElSize(x), setElSize(y));
	};
};

function setMachinePos(id, x, y){
	var url = ctxPath + "/svg/setMachinePos.do";
	var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){}
	});
};
