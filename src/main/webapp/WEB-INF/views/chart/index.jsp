<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg_controller.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>

<style>

#container{
	background-color: black;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}


</style>
<script type="text/javascript">

	$(window).resize(function() {
		location.reload();
	});
		
	$(function(){
		$("#logout, #id_font, #appVer").remove()
		setDate();
		$("#menu_btn").click(function(){
			location.href = "/iDOO_CONTROL/chart/main.do";
		});
		
		setEl();
		getLineStatus();
	});
	
	function addZero(str){
		if(str.length==1){
			str = "0" + str;
		};
		return str;
	};
	
	function setDate(){
		setTimeout(setDate, 1000);
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));		
		var dayOfWeek;
		
		if(date.getDay() == 0){
			dayOfWeek = "일";
		}else if(date.getDay() == 1){
			dayOfWeek = "월";
		}else if(date.getDay() == 2){
			dayOfWeek = "화";
		}else if(date.getDay() == 3){
			dayOfWeek = "수";
		}else if(date.getDay() == 4){
			dayOfWeek = "목";
		}else if(date.getDay() == 5){
			dayOfWeek = "금";
		}else if(date.getDay() == 6){
			dayOfWeek = "토";
		}else{
			dayOfWeek = "?";
		}
		
		var today = year + ". " + month + ". " + day + ".(" + dayOfWeek + ")&nbsp&nbsp&nbsp&nbsp" + hour + ":" + minute; 
		var type;
		if(((Number(hour) * 60) + Number(minute) >= 510) && ((Number(hour) * 60) + Number(minute) <= 1230)){
			type = "${day}";
			tagetType = 2;
		}else{
			type = "${night}";
			tagetType = 1;
		};
		
		$("#title").css({
			"font-size" : getElSize(150) + "px",
			"color" : "yellow"
		});
		
		$("#time").html(today).css({
			"font-size" : getElSize(150) + "px",
			"font-weight" : "bolder",
			"color" : "yellow",
			"margin-left" : getElSize(500)
		});
		
		$(".neon").css({
			"font-size" : getElSize(120) + "px",
			"text-shadow" :  getElSize(-10) + "px 0 black, 0 " + getElSize(10) + "px black, " + getElSize(10) + "px 0 black, 0 " + getElSize(-10) + "px black",
			//"margin-top" : getElSize(60),
		});
				
		$("#alarm").css({
			"text-shadow" :  getElSize(-10) + "px 0 black, 0 " + getElSize(10) + "px black, " + getElSize(10) + "px 0 black, 0 " + getElSize(-10) + "px black",
			"font-size" : getElSize(130),
			"font-weight" : "bolder"
		});
	};
	
	// wilson
	function getLineStatus(){

		var url = ctxPath + "/getLineStatus.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				//console.log(data);
				
				$(json).each(function(idx, data){
					var loader = ctxPath + "/images/DashBoard/GAN_LOADER-";
					var inspec = ctxPath + "/images/DashBoard/INSPEC_DEVICE_";
					
					if(data.line == "A"){
						target = "#target_A";
						good = "#good_A";
						bad = "#bad_A";
						gan = "#gan_A";
						ratio = "#ratio_A";
					}else if(data.line == "B"){
						target = "#target_B";
						good = "#good_B";
						bad = "#bad_B";
						gan = "#gan_B";
						ratio = "#ratio_B";
					}else if(data.line == "C"){
						target = "#target_C";
						good = "#good_C";
						bad = "#bad_C";
						gan = "#gan_C";
						ratio = "#ratio_C";
					}else if(data.line == "D"){
						target = "#target_D";
						good = "#good_D";
						bad = "#bad_D";
						gan = "#gan_D";
						ratio = "#ratio_D";
					}else if(data.line == "E"){
						target = "#target_E";
						good = "#good_E";
						bad = "#bad_E";
						gan = "#gan_E";
						ratio = "#ratio_E";
					}
										
					loader += data.loader;
					loader += ".png";
					
					inspec += data.loader;
					inspec += ".png";
					
					$(target).html(data.target);
					$(good).html(data.good);
					$(bad).html(data.bad);
					
					// 최근 값을 받은 시간과 현재 시간의 차이가 20분이 지나면, 상태를 NO-CON으로 바꾸기 
					var reg = new Date(data.regdt);
					var now = new Date();
					var gap = ((now.getTime() - reg.getTime())/1000/60).toFixed(1)
					
					if(data.line == 'E'){
						
						//console.log("regdt : " + reg);
						//console.log("gap : " + ((now.getTime() - reg.getTime())/1000/60).toFixed(1) );
						
						if(gap >= 20){
							$(gan).attr("src", ctxPath + "/images/DashBoard/INSPEC_DEVICE_NO-CON.png");
						}else{
							$(gan).attr("src", inspec);	
						}
						
					}else{
						if(gap >= 20){
							$(gan).attr("src", ctxPath + "/images/DashBoard/GAN_LOADER-NO-CON.png");
						}else{
							$(gan).attr("src", loader);
						}
					}
					
					var num = (data.good / data.target * 100)
					var tmp = num + "";
					
					if(tmp.indexOf(".") != -1){
						num = (data.good / data.target * 100).toFixed(1)
					}
					
					$(ratio).html(num + "%");
				});
			}
		});
		
		// 주기 
		setTimeout(getLineStatus, 3000);
	}
	
	// 소수점 뒷자리 쓸데없는 "0" 제거하기
	function ToFloat(number){
			
		var tmp = number + "";

		if(tmp.indexOf(".") != -1){			
			number = number.toFixed(1);
			number = number.replace(/(0+$)/, "");
		}
		return number;
	}
	
	function setEl(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			"width" : originWidth,
			"height" : originHeight,
		});
		
		$(".title").css({
			"font-size" : getElSize(80) + "px",
			"color" : "white",
			"font-weight" : "bolder"
		});
		
		$(".div").css({
			"margin-top" : getElSize(80),
			"width" : getElSize(2000)
		});
				
		$("#mainTable").css({
			"border-radius" : getElSize(20),
			"height" : originHeight
		});
		
		$("#mainTable td").css({
			//"padding" : getElSize(25),
			"border" : getElSize(10) + "px solid yellow"
		});
		
		$("#flagDiv").css({
			"top": getElSize(10),
			"left": getElSize(10),
			"z-index" : "13"
		});
		
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(130),
			"left" : getElSize(10),
			"top" : getElSize(120),
			"cursor" : "pointer",
			"z-index" : 13
		});
		
		$("#title_tr").css({
			"height" : getElSize(76)
		});
		
		$("#mainTable tr:nth-child(1)").css({
			"height" : getElSize(318)
		});
		
		$("#mainTable tr:nth-child(2)").css({
			"height" : getElSize(76)
		});
		
		$("#mainTable tr:nth-child(3)").css({
			"height" : getElSize(300)
		});
		
		$("#mainTable tr:nth-child(4)").css({
			"height" : getElSize(300)
		});
		
		$("#mainTable tr:nth-child(5)").css({
			"height" : getElSize(300)
		});
		
		$("#mainTable tr:nth-child(6)").css({
			"height" : getElSize(300)
		});
		
		$("#mainTable tr:nth-child(7)").css({
			"height" : getElSize(300)
		});
		
		$(".line").css({
			"font-size" : getElSize(120) + "px",
			"color" : "white"
		});
		
		$("#t1").css({
			"width" : getElSize(320),
			"height" : getElSize(100)
		})

		$("#t2").css({
			"width" : getElSize(1800)
		})
		/*
		$("#t3").css({
			"width" : getElSize(450)
		})
		
		$("#t4").css({
			"width" : getElSize(450)
		})
		
		$("#t5").css({
			"width" : getElSize(450)
		})
		
		$("#t6").css({
			"width" : getElSize(550)
		})*/
		
		$("#svg").css({
			/* "width" : $("#table").width() - $("#svg_td").offset().left + marginWidth , */
			"width" : $("#mainTable").width(), // + getElSize(216),// + marginWidth,
			"height" : $("#mainTable").height(),//originHeight,
			"left" : $("#mainTable").offset().left, // - getElSize(109),
			"position" :"absolute",
			"z-index" : 9
		});
		
		$("#gan_A").css({
			"width" : getElSize(1620),
			"height" : getElSize(103),
			"margin-top" : getElSize(15)
		})
		
		$("#gan_B").css({
			"width" : getElSize(1620),
			"height" : getElSize(103),
			"margin-bottom" : getElSize(15)
		})
		
		$("#gan_C").css({
			"width" : getElSize(1620),
			"height" : getElSize(103),
			"margin-top" : getElSize(15)
		})
		
		$("#gan_D").css({
			"width" : getElSize(1620),
			"height" : getElSize(103),
			"margin-bottom" : getElSize(15)
		})
		
		$("#gan_E").css({
			"width" : getElSize(1620),
			"height" : getElSize(200),
			//"display" : "none"
		})
		
		$(".target").css({
			"color" : "white"
		})
		
		$(".good").css({
			"color" : "cyan"
		})
		
		$(".bad").css({
			"color" : "red"
		})
		
		$(".ratio").css({
			"color" : "#ABF200"
		})
	};
</script>
</head>
<body>
	<div id="svg"></div>
	<div id="container">
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
		<div id="wrapper">
			<center>
				<table style="border-collapse: collapse; word-break:break-all; table-layout: fixed; width: 100%;" id="mainTable">	<!-- width: 100%;  -->
					<colgroup>
						<col id="t1">
						<col id="t2">
						<col id="t3">
						<col id="t4">
						<col id="t5">
						<col id="t6">
					</colgroup> 
					<Tr>
						<Td colspan="6" style="text-align: center;">
							<span id="title">라인 모니터링</span>
							<span id="time"></span>
						</Td>
					</Tr>
					<Tr id="title_tr">
						<Td style="text-align: center;vertical-align: middle;"><span class="title">라인</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="title">라인 상태</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="title"><spring:message code="target_cnt"></spring:message></span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="title"><spring:message code="prd_cnt"></spring:message></span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="title">불량</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="title"><spring:message code="target_ratio"></spring:message></span></Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;vertical-align: middle;"><span class="line">A</span></Td>
						<Td style="text-align: center;vertical-align: top;"><img alt="" id="gan_A"></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon target" id="target_A">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon good" id="good_A">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon bad" id="bad_A">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon ratio" id="ratio_A">0%</span></Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;vertical-align: middle;"><span class="line">B</span></Td>
						<Td style="text-align: center;vertical-align: bottom;"><img alt="" id="gan_B"></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon target" id="target_B">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon good" id="good_B">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon bad" id="bad_B">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon ratio" id="ratio_B">0%</span></Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;vertical-align: middle;"><span class="line">C</span></Td>
						<Td style="text-align: center;vertical-align: top;"><img alt="" id="gan_C"></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon target" id="target_C">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon good" id="good_C">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon bad" id="bad_C">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon ratio" id="ratio_C">0%</span></Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;vertical-align: middle;"><span class="line">D</span></Td>
						<Td style="text-align: center;vertical-align: bottom;"><img alt="" id="gan_D"></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon target" id="target_D">50</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon good" id="good_D">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon bad" id="bad_D">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon ratio" id="ratio_D">0%</span></Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;vertical-align: middle;"><span class="line">전조/검사</span></Td>	<!-- style="max-height:50px" -->
						<Td style="text-align: center;vertical-align: middle;"><img alt="" id="gan_E"></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon target" id="target_E">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon good" id="good_E">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon bad" id="bad_E">0</span></Td>
						<Td style="text-align: center;vertical-align: middle;"><span class="icon- neon ratio" id="ratio_E">0%</span></Td>
					</Tr>
				</table>
			</center>
		</div>
	</div>
</body>
</html>