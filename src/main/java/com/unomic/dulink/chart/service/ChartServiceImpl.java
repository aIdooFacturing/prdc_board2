package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;
 

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService{
	private final static String namespace= "com.unomic.dulink.chart.";
	private static final Logger logger = LoggerFactory.getLogger(ChartServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSessionTemplate_app_server")
	private SqlSession app_server_sql;
	
	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}
	
	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo),"utf-8");
		return str;
	}
	
	
	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		
		int exist = (int) sql.selectOne(namespace + "login", chartVo);
		
		if(exist==0){
			str = "fail";
		}else{
			str = "success";
		};
		
		return str;
	};
	
	
	@Override
	public String getAppList(ChartVo chartVo) throws Exception {
		List <ChartVo> dataList  = app_server_sql.selectList(namespace + "getAppList", chartVo);
	    
		List list = new ArrayList<ChartVo>();
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			
			map.put("id", dataList.get(i).getId());
			map.put("appId", dataList.get(i).getAppId());
			map.put("name", dataList.get(i).getAppName());
			map.put("url", dataList.get(i).getUrl());
			
			list.add(map);
		}; 
  
		Map dataMap = new HashMap(); 
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String removeApp(ChartVo chartVo) throws Exception {
		String str = "";
		try{
			app_server_sql.delete(namespace + "removeApp", chartVo);
			str = "success";
		}catch(Exception e){
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}       

	@Override
	public String addNewApp(ChartVo chartVo) throws Exception {
		String str = "";
		try{
			app_server_sql.delete(namespace + "addNewApp", chartVo);
			str = "success";
		}catch(Exception e){
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	//common func
	

	
	@Override
	public ChartVo getData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getData", chartVo);

		return chartVo;
	}
	
	@Override
	public String getDvcIdForSign(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcIdForSign", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getDvcId());
			map.put("status", dataList.get(i).getStatus());
			map.put("name", URLEncoder.encode(dataList.get(i).getName()));
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());

			list.add(map);
		};

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}
	
	
	@Override
	public String getDvcSelId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcSelId", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getDvcId());
			
			list.add(map);
		};

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);
		
		//logger.info("list : " + list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}
	
	
	@Override
	public String getCirTime() throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getCirTime");
		
		return rtn;
	}
	
	
	@Override
	public String setLineStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		String result = "";
		try {
			sql.update(namespace + "setLineStatus", chartVo);
			result = "success";
		}catch(Exception e) {
			e.printStackTrace();
			result = "fail";
		}
		return result;
	}
	
	
	@Override
	public String getLineStatus() throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> dataList = sql.selectList(namespace + "getLineStatus");
		
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			
			map.put("line", dataList.get(i).getLine());
			map.put("loader", dataList.get(i).getLoader());
			map.put("target", dataList.get(i).getTarget());
			map.put("good", dataList.get(i).getGood());
			map.put("bad", dataList.get(i).getBad());
			map.put("regdt", dataList.get(i).getRegdt());
			
			list.add(map);
		};
		
		//logger.info("dataList : " + dataList);
		//logger.info("list : " + list);
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		
		//logger.info(str);
		return str;
	}
	

};