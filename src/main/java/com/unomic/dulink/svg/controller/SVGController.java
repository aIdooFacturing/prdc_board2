package com.unomic.dulink.svg.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.svg.domain.SVGVo;
import com.unomic.dulink.svg.service.SVGService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/svg")
@Controller
public class SVGController {

	private static final Logger logger = LoggerFactory.getLogger(SVGController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private SVGService svgService; 

	@RequestMapping(value="getMachineInfo")
	@ResponseBody
	public String getMachineInfo(SVGVo svgVo) {
		String machine = ""; 
		
		try {
			machine = svgService.getMachineInfo(svgVo);
		} catch (Exception e) {
			e.printStackTrace();
		};
		
		return machine;
	};
	
	@RequestMapping(value="getMarker")
	@ResponseBody
	public SVGVo getMarker() {
		SVGVo svg = new SVGVo();
		try {
			svg = svgService.getMarker();
		} catch (Exception e) {
			e.printStackTrace();
		};
		
		return svg;
	};
	
	@RequestMapping(value="setMachinePos")
	@ResponseBody
	public void setMachinePos(SVGVo svgVo){
		try {
			svgService.setMachinePos(svgVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	};
	
};

